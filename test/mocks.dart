import 'package:flutter/widgets.dart';
import 'package:mockito/mockito.dart';
import 'package:graphql/client.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/api/authentication_api.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

class MockGraphQLClient extends Mock implements GraphQLClient {}

class MockAccountingApi extends Mock implements AccountingApi {}
class MockAuthenticationApi extends Mock implements AuthenticationApi {}
