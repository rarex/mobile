import 'package:flutter_test/flutter_test.dart';
import 'package:graphql/client.dart';
import 'package:mockito/mockito.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/models/account.dart';

import '../mocks.dart';

void main() {
  GraphQLClient mockGraphQLClient;
  AccountingApi api;

  setUp(() {
    mockGraphQLClient = MockGraphQLClient();
    api = AccountingApi(mockGraphQLClient);
  });

  group('Accounting listAccounts tests', () {
    test('listAccounts returns list of accounts', () async {
      final QueryResult mockResult = QueryResult(
        data: <String, dynamic>{
          'accounts': <dynamic>[
            <String, dynamic>{
              'balance': '0E-10',
              'currency': <String, String>{
                'code': 'TEST',
                'name': 'Test Currency',
              },
            },
            <String, dynamic>{
              'balance': '0E-10',
              'currency': <String, String>{
                'code': 'SMPL',
                'name': 'Sample Currency',
              },
            },
          ],
        },
      );

      when(mockGraphQLClient.query(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final List<Account> accounts = await api.listAccounts();

      verify(mockGraphQLClient.query(any));
      expect(accounts.length, 2);
    });
  });
}
