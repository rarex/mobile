import 'package:flutter_test/flutter_test.dart';
import 'package:graphql/client.dart';
import 'package:mockito/mockito.dart';

import 'package:rarex/api/authentication_api.dart';
import 'package:rarex/models/session.dart';
import 'package:rarex/models/user.dart';

import '../mocks.dart';

void main() {
  GraphQLClient mockGraphQLClient;
  AuthenticationApi api;

  setUp(() {
    mockGraphQLClient = MockGraphQLClient();
    api = AuthenticationApi(mockGraphQLClient);
  });

  group('Authentication signIn tests', () {
    test(
        'signIn with valid credentials returns successful Session with unconfirmed user',
        () async {
      final QueryResult mockResult = QueryResult(
        data: <String, dynamic>{
          'signIn': <String, dynamic>{
            'authenticationToken': 'abcd1234',
            'user': <String, String>{
              'id': '1234',
              'email': 'test@example.com',
              'firstName': 'Test',
              'lastName': 'User',
              'insertedAt': '2019-06-01T08:00:20',
            },
          },
        },
      );

      when(mockGraphQLClient.mutate(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final Session session = await api.signIn('test@example.com', 'password');

      verify(mockGraphQLClient.mutate(any));
      expect(session.successful, true);
      expect(session.authenticationToken, 'abcd1234');
      expect(session.user.id, 1234);
      expect(session.user.email, 'test@example.com');
      expect(session.user.firstName, 'Test');
      expect(session.user.lastName, 'User');
      expect(session.user.registeredAt, DateTime.parse('2019-06-01T08:00:20'));
      expect(session.user.confirmedAt, null);
    });

    test(
        'signIn with valid credentials returns successful Session with confirmed user',
        () async {
      final QueryResult mockResult = QueryResult(
        data: <String, dynamic>{
          'signIn': <String, dynamic>{
            'authenticationToken': 'abcd1234',
            'user': <String, String>{
              'id': '1234',
              'email': 'test@example.com',
              'firstName': 'Test',
              'lastName': 'User',
              'insertedAt': '2019-06-01T08:00:20',
              'confirmedAt': '2019-06-01T08:00:20',
            },
          },
        },
      );

      when(mockGraphQLClient.mutate(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final Session session = await api.signIn('test@example.com', 'password');

      verify(mockGraphQLClient.mutate(any));
      expect(session.successful, true);
      expect(session.authenticationToken, 'abcd1234');
      expect(session.user.id, 1234);
      expect(session.user.email, 'test@example.com');
      expect(session.user.firstName, 'Test');
      expect(session.user.lastName, 'User');
      expect(session.user.registeredAt, DateTime.parse('2019-06-01T08:00:20'));
      expect(session.user.confirmedAt, DateTime.parse('2019-06-01T08:00:20'));
    });

    test('signIn with non-existing credentials returns failed Session',
        () async {
      final QueryResult mockResult = QueryResult(
        errors: <GraphQLError>[
          GraphQLError(message: 'no user with matching credentials found'),
        ],
      );

      when(mockGraphQLClient.mutate(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final Session session = await api.signIn('test@example.com', 'password');

      verify(mockGraphQLClient.mutate(any));
      expect(session.successful, false);
      expect(
          session.errors[0].message, 'no user with matching credentials found');
    });
  });

  group('Authentication signUp tests', () {
    test('signUp with valid credentials returns successful Session', () async {
      final QueryResult mockResult = QueryResult(
        data: <String, dynamic>{
          'signUp': <String, dynamic>{
            'authenticationToken': 'abcd1234',
            'user': <String, String>{
              'id': '1234',
              'email': 'test@example.com',
              'firstName': 'Test',
              'lastName': 'User',
              'insertedAt': '2019-06-01T08:00:20',
            },
          },
        },
      );

      when(mockGraphQLClient.mutate(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final Session session = await api.signUp(
          'test@example.com', 'password', 'password', 'Test', 'User');

      verify(mockGraphQLClient.mutate(any));
      expect(session.successful, true);
      expect(session.authenticationToken, 'abcd1234');
      expect(session.user.id, 1234);
      expect(session.user.email, 'test@example.com');
      expect(session.user.firstName, 'Test');
      expect(session.user.lastName, 'User');
      expect(session.user.registeredAt, DateTime.parse('2019-06-01T08:00:20'));
    });

    test('signUp with already existing email returns failed Session', () async {
      final QueryResult mockResult = QueryResult(
        errors: <GraphQLError>[
          GraphQLError(message: 'email has already been taken'),
        ],
      );

      when(mockGraphQLClient.mutate(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final Session session = await api.signUp(
          'test@example.com', 'password', 'password', 'Test', 'User');

      verify(mockGraphQLClient.mutate(any));
      expect(session.successful, false);
      expect(session.errors[0].message, 'email has already been taken');
    });
  });

  group('Authentication confirmUser tests', () {
    test('confirmUser with valid confirmation code returns confirmed User',
        () async {
      final QueryResult mockResult = QueryResult(
        data: <String, dynamic>{
          'confirmUser': <String, String>{
            'id': '1234',
            'email': 'test@example.com',
            'firstName': 'Test',
            'lastName': 'User',
            'insertedAt': '2019-06-01T08:00:20',
            'confirmedAt': '2019-06-01T08:00:20',
          },
        },
      );

      when(mockGraphQLClient.mutate(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final User user = await api.confirmUser('1234');

      verify(mockGraphQLClient.mutate(any));
      expect(user.id, 1234);
      expect(user.email, 'test@example.com');
      expect(user.firstName, 'Test');
      expect(user.lastName, 'User');
      expect(user.registeredAt, DateTime.parse('2019-06-01T08:00:20'));
      expect(user.confirmedAt, DateTime.parse('2019-06-01T08:00:20'));
    });

    test('confirmUser with invalid confirmation code returns empty User',
        () async {
      final QueryResult mockResult = QueryResult(
        errors: <GraphQLError>[
          GraphQLError(message: 'invalid confirmation code'),
        ],
      );

      when(mockGraphQLClient.mutate(any))
          .thenAnswer((_) => Future<QueryResult>.value(mockResult));

      final User user = await api.confirmUser('1234');

      verify(mockGraphQLClient.mutate(any));
      expect(user.id, null);
      expect(user.email, null);
      expect(user.firstName, null);
      expect(user.lastName, null);
      expect(user.registeredAt, null);
      expect(user.confirmedAt, null);
    });
  });
}
