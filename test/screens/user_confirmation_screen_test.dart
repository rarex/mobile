import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/api/authentication_api.dart';
import 'package:rarex/models/account.dart';
import 'package:rarex/models/user.dart';
import 'package:rarex/screens/accounts_screen.dart';
import 'package:rarex/screens/landing_screen.dart';
import 'package:rarex/screens/user_confirmation_screen.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/utils/app_config.dart';
import 'package:rarex/widgets/app.dart';

import '../mocks.dart';

void main() {
  group('User Confirmation Screen tests', () {
    NavigatorObserver mockNavigatorObserver;
    AccountingApi mockAccountingApi;
    AuthenticationApi mockAuthenticationApi;

    const String sessionString = '''
    {
      "successful":true,
      "authenticationToken":"abcd1234",
      "user":{
        "id":"1",
        "email":"test@example.com",
        "firstName":"Test",
        "lastName":"User",
        "registeredAt":"2019-06-01 08:00:20.000"
      }
    }
    ''';

    setUpAll(() async {
      mockNavigatorObserver = MockNavigatorObserver();
      mockAccountingApi = MockAccountingApi();
      mockAuthenticationApi = MockAuthenticationApi();

      SharedPreferences.setMockInitialValues(<String, dynamic>{
        'flutter.currentSession': sessionString,
      });
    });

    tearDown(() async {
      // Yes, we actually have to revert SharedPreference manually
      // because it persists across tests depsite doing the initialization
      // step inside setUp(). No, there still is no solution to this.
      final SharedPreferences preferences =
          await SharedPreferences.getInstance();
      await preferences.remove('currentSession');
      await preferences.setString('currentSession', sessionString);
    });

    Future<void> _buildTestApp(WidgetTester tester) async {
      final AppConfig appConfig = AppConfig(
        apiHost: 'http://localhost:4000/api',
        appName: 'Rarex TEST',
        flavorName: 'test',
      );

      final Widget testApp = Provider<AppConfig>.value(
        value: appConfig,
        child: MultiProvider(
          providers: <SingleChildCloneableWidget>[
            Provider<ApplicationStore>(
              builder: (_) => ApplicationStore(),
            ),
            Provider<AuthenticationStore>(
              builder: (_) => AuthenticationStore(),
            ),
            Provider<GlobalKey<NavigatorState>>(
              builder: (_) => GlobalKey<NavigatorState>(),
            ),
            ProxyProvider2<ApplicationStore, AuthenticationStore,
                ApplicationService>(
              builder: (_, ApplicationStore applicationStore,
                  AuthenticationStore authenticationStore, __) {
                return ApplicationService(
                    applicationStore, authenticationStore);
              },
            ),
          ],
          child: App.forTesting(
            navigatorObserver: mockNavigatorObserver,
            customAccountingApi: mockAccountingApi,
            customAuthenticationApi: mockAuthenticationApi,
          ),
        ),
      );

      await tester.pumpWidget(testApp);
      await tester.pumpAndSettle();
    }

    testWidgets(
        'Submitting form with valid code transitions to Accounts Screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      final List<Account> accounts = <Account>[];
      final User user = User(
        id: 1234,
        email: 'test@example.com',
        firstName: 'Test',
        lastName: 'User',
        registeredAt: DateTime.parse('2019-06-01T08:00:20'),
        confirmedAt: DateTime.parse('2019-06-01T08:00:20'),
      );

      when(mockAccountingApi.listAccounts()).thenAnswer((_) {
        return Future<List<Account>>.value(accounts);
      });

      when(mockAuthenticationApi.confirmUser(any)).thenAnswer((_) {
        return Future<User>.value(user);
      });

      await tester.enterText(
          find.byKey(UserConfirmationFormState.confirmationCodeFormFieldKey),
          '1234');
      await tester.tap(find.byKey(UserConfirmationFormState.confirmButtonKey));
      await tester.pumpAndSettle();

      verify(mockAuthenticationApi.confirmUser(any));
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.byType(AccountsScreen), findsOneWidget);
    });

    testWidgets('Submitting form with invalid code returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      final User user = User();

      when(mockAuthenticationApi.confirmUser(any))
          .thenAnswer((_) => Future<User>.value(user));

      await tester.enterText(
          find.byKey(UserConfirmationFormState.confirmationCodeFormFieldKey),
          '1234');
      await tester.tap(find.byKey(UserConfirmationFormState.confirmButtonKey));
      await tester.pumpAndSettle();

      verify(mockAuthenticationApi.confirmUser(any));
      expect(find.byType(AccountsScreen), findsNothing);
      expect(find.byType(UserConfirmationScreen), findsOneWidget);
      expect(find.text('Verification code is incorrect'), findsOneWidget);
    });

    testWidgets('Tapping sign out button transitions to Landing screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.tap(find.byKey(UserConfirmationFormState.signOutButtonKey));
      await tester.pumpAndSettle();

      verifyNever(mockAuthenticationApi.confirmUser(any));
      expect(find.byType(AccountsScreen), findsNothing);
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.byType(LandingScreen), findsOneWidget);
    });
  });
}
