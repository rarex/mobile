import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:decimal/decimal.dart';
import 'package:mockito/mockito.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/models/account.dart';
import 'package:rarex/models/currency.dart';
import 'package:rarex/screens/accounts_screen.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/utils/app_config.dart';
import 'package:rarex/widgets/app.dart';

import '../mocks.dart';

void main() {
  group('Accounts Screen tests', () {
    AccountingApi mockAccountingApi;

    setUp(() {
      mockAccountingApi = MockAccountingApi();

      const String sessionString = '''
      {
        "successful":true,
        "authenticationToken":"abcd1234",
        "user":{
          "id":"1",
          "email":"test@example.com",
          "firstName":"Test",
          "lastName":"User",
          "registeredAt":"2019-06-01 08:00:20.000",
          "confirmedAt":"2019-06-01 08:00:20.000"
        }
      }
      ''';

      SharedPreferences.setMockInitialValues(<String, dynamic>{
        'flutter.currentSession': sessionString,
      });
    });

    Future<void> _buildTestApp(WidgetTester tester) async {
      final AppConfig appConfig = AppConfig(
        apiHost: 'http://localhost:4000/api',
        appName: 'Rarex TEST',
        flavorName: 'test',
      );

      final Widget testApp = Provider<AppConfig>.value(
        value: appConfig,
        child: MultiProvider(
          providers: <SingleChildCloneableWidget>[
            Provider<ApplicationStore>(
              builder: (_) => ApplicationStore(),
            ),
            Provider<AuthenticationStore>(
              builder: (_) => AuthenticationStore(),
            ),
            Provider<GlobalKey<NavigatorState>>(
              builder: (_) => GlobalKey<NavigatorState>(),
            ),
            ProxyProvider2<ApplicationStore, AuthenticationStore,
                ApplicationService>(
              builder: (_, ApplicationStore applicationStore,
                  AuthenticationStore authenticationStore, __) {
                return ApplicationService(
                    applicationStore, authenticationStore);
              },
            ),
          ],
          child: App.forTesting(
            customAccountingApi: mockAccountingApi,
          ),
        ),
      );

      await tester.pumpWidget(testApp);
      await tester.pumpAndSettle();
    }

    testWidgets('Accounts Screen with user accounts renders accounts',
        (WidgetTester tester) async {
      final List<Account> accounts = <Account>[
        Account(
          balance: Decimal.parse('1234.56'),
          currency: Currency(
            code: 'TEST',
            name: 'Test Currency'
          ),
        ),
        Account(
          balance: Decimal.parse('12.3456789'),
          currency: Currency(
            code: 'SMPL',
            name: 'Sample Currency'
          ),
        ),
      ];

      when(mockAccountingApi.listAccounts()).thenAnswer((_) {
        return Future<List<Account>>.value(accounts);
      });

      await _buildTestApp(tester);

      expect(find.byType(AccountsScreen), findsOneWidget);
      expect(find.byType(AccountItem), findsNWidgets(2));
    });

    testWidgets('Accounts Screen without user accounts renders no account text',
        (WidgetTester tester) async {
      final List<Account> accounts = <Account>[];

      when(mockAccountingApi.listAccounts()).thenAnswer((_) {
        return Future<List<Account>>.value(accounts);
      });

      await _buildTestApp(tester);

      expect(find.byType(AccountsScreen), findsOneWidget);
      expect(find.byType(AccountItem), findsNothing);
      expect(find.text('You have no accounts'), findsOneWidget);
    });
  });
}
