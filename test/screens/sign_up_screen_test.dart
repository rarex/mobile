import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:mockito/mockito.dart';
import 'package:graphql/client.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:rarex/api/authentication_api.dart';
import 'package:rarex/models/session.dart';
import 'package:rarex/models/user.dart';
import 'package:rarex/screens/landing_screen.dart';
import 'package:rarex/screens/sign_up_screen.dart';
import 'package:rarex/screens/user_confirmation_screen.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/utils/app_config.dart';
import 'package:rarex/widgets/app.dart';

import '../mocks.dart';

void main() {
  group('Sign Up Screen tests', () {
    NavigatorObserver mockNavigatorObserver;
    AuthenticationApi mockAuthenticationApi;

    setUp(() {
      mockNavigatorObserver = MockNavigatorObserver();
      mockAuthenticationApi = MockAuthenticationApi();
      SharedPreferences.setMockInitialValues(<String, dynamic>{});
    });

    Future<void> _buildTestApp(WidgetTester tester) async {
      final AppConfig appConfig = AppConfig(
        apiHost: 'http://localhost:4000/api',
        appName: 'Rarex TEST',
        flavorName: 'test',
      );

      final Widget testApp = Provider<AppConfig>.value(
        value: appConfig,
        child: MultiProvider(
          providers: <SingleChildCloneableWidget>[
            Provider<ApplicationStore>(
              builder: (_) => ApplicationStore(),
            ),
            Provider<AuthenticationStore>(
              builder: (_) => AuthenticationStore(),
            ),
            Provider<GlobalKey<NavigatorState>>(
              builder: (_) => GlobalKey<NavigatorState>(),
            ),
            ProxyProvider2<ApplicationStore, AuthenticationStore,
                ApplicationService>(
              builder: (_, ApplicationStore applicationStore,
                  AuthenticationStore authenticationStore, __) {
                return ApplicationService(
                    applicationStore, authenticationStore);
              },
            ),
          ],
          child: App.forTesting(
              navigatorObserver: mockNavigatorObserver, customAuthenticationApi: mockAuthenticationApi),
        ),
      );

      await tester.pumpWidget(testApp);
      await tester.tap(find.byKey(LandingScreen.navigateToSignUpButtonKey));
      await tester.pumpAndSettle();
    }

    testWidgets(
        'Submitting form with valid data and existing user transitions to Accounts Screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      final Session session = Session(
        successful: true,
        authenticationToken: 'abcd1234',
        user: User(
            id: 1234,
            email: 'test@example.com',
            firstName: 'Test',
            lastName: 'User',
            registeredAt: DateTime.parse('2019-06-01T08:00:20')),
      );
      when(mockAuthenticationApi.signUp(any, any, any, any, any))
          .thenAnswer((_) => Future<Session>.value(session));

      await tester.enterText(find.byKey(SignUpFormState.emailTextFormFieldKey),
          'test@example.com');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordTextFormFieldKey), 'password');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordConfirmationTextFormFieldKey),
          'password');
      await tester.enterText(
          find.byKey(SignUpFormState.firstNameTextFormFieldKey), 'Test');
      await tester.enterText(
          find.byKey(SignUpFormState.lastNameTextFormFieldKey), 'User');
      await tester.tap(find.byKey(SignUpFormState.signUpButtonKey));
      await tester.pumpAndSettle();

      verify(mockAuthenticationApi.signUp(any, any, any, any, any));
      verify(mockNavigatorObserver.didPop(any, any));
      expect(find.byType(SignUpScreen), findsNothing);
      expect(find.byType(UserConfirmationScreen), findsOneWidget);
    });

    testWidgets('Submitting form with already existing email returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      final Session session = Session(
        successful: false,
        errors: <GraphQLError>[
          GraphQLError(message: 'email has already been taken'),
        ],
      );
      when(mockAuthenticationApi.signUp(any, any, any, any, any))
          .thenAnswer((_) => Future<Session>.value(session));

      await tester.enterText(find.byKey(SignUpFormState.emailTextFormFieldKey),
          'test@example.com');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordTextFormFieldKey), 'password');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordConfirmationTextFormFieldKey),
          'password');
      await tester.enterText(
          find.byKey(SignUpFormState.firstNameTextFormFieldKey), 'Test');
      await tester.enterText(
          find.byKey(SignUpFormState.lastNameTextFormFieldKey), 'User');
      await tester.tap(find.byKey(SignUpFormState.signUpButtonKey));
      await tester.pumpAndSettle();

      verify(mockAuthenticationApi.signUp(any, any, any, any, any));
      verifyNever(mockNavigatorObserver.didPop(any, any));
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.byType(SignUpScreen), findsOneWidget);
      expect(find.text('email has already been taken'), findsOneWidget);
    });

    testWidgets('Submitting form with invalid email address returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.enterText(
          find.byKey(SignUpFormState.emailTextFormFieldKey), 'test@example');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordTextFormFieldKey), 'password');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordConfirmationTextFormFieldKey),
          'password');
      await tester.enterText(
          find.byKey(SignUpFormState.firstNameTextFormFieldKey), 'Test');
      await tester.enterText(
          find.byKey(SignUpFormState.lastNameTextFormFieldKey), 'User');
      await tester.tap(find.byKey(SignUpFormState.signUpButtonKey));
      await tester.pumpAndSettle();

      expect(find.byType(SignUpScreen), findsOneWidget);
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.text('Please enter a valid email address'), findsOneWidget);
      expect(find.text('Please enter your password'), findsNothing);
      expect(find.text('Please confirm your password'), findsNothing);
    });

    testWidgets(
        'Submitting form with invalid non-matching password returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.enterText(
          find.byKey(SignUpFormState.emailTextFormFieldKey), 'test@example');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordTextFormFieldKey), 'password');
      await tester.enterText(
          find.byKey(SignUpFormState.passwordConfirmationTextFormFieldKey),
          'password1');
      await tester.enterText(
          find.byKey(SignUpFormState.firstNameTextFormFieldKey), 'Test');
      await tester.enterText(
          find.byKey(SignUpFormState.lastNameTextFormFieldKey), 'User');
      await tester.tap(find.byKey(SignUpFormState.signUpButtonKey));
      await tester.pumpAndSettle();

      expect(find.byType(SignUpScreen), findsOneWidget);
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.text('Password confirmation does not match'), findsOneWidget);
      expect(find.text('Please enter your email address'), findsNothing);
      expect(find.text('Please enter your password'), findsNothing);
      expect(find.text('Please confirm your password'), findsNothing);
    });

    testWidgets('Submitting form without entering anything returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.tap(find.byKey(SignUpFormState.signUpButtonKey));
      await tester.pumpAndSettle();

      expect(find.byType(SignUpScreen), findsOneWidget);
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.text('Please enter your email address'), findsOneWidget);
      expect(find.text('Please enter your password'), findsOneWidget);
      expect(find.text('Please enter your first name'), findsOneWidget);
      expect(find.text('Please enter your last name'), findsOneWidget);
      expect(find.text('Please confirm your password'), findsOneWidget);
    });
  });
}
