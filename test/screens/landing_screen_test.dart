import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:rarex/screens/landing_screen.dart';
import 'package:rarex/screens/sign_in_screen.dart';
import 'package:rarex/screens/sign_up_screen.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/utils/app_config.dart';
import 'package:rarex/widgets/app.dart';

import '../mocks.dart';

void main() {
  group('Landing Screen tests', () {
    NavigatorObserver mockNavigatorObserver;

    setUp(() {
      mockNavigatorObserver = MockNavigatorObserver();
      SharedPreferences.setMockInitialValues(<String, dynamic>{});
    });

    Future<void> _buildTestApp(WidgetTester tester) async {
      final AppConfig appConfig = AppConfig(
        apiHost: 'http://localhost:4000/api',
        appName: 'Rarex TEST',
        flavorName: 'test',
      );

      final Widget testApp = Provider<AppConfig>.value(
        value: appConfig,
        child: MultiProvider(
          providers: <SingleChildCloneableWidget>[
            Provider<ApplicationStore>(
              builder: (_) => ApplicationStore(),
            ),
            Provider<AuthenticationStore>(
              builder: (_) => AuthenticationStore(),
            ),
            Provider<GlobalKey<NavigatorState>>(
              builder: (_) => GlobalKey<NavigatorState>(),
            ),
            ProxyProvider2<ApplicationStore, AuthenticationStore,
                ApplicationService>(
              builder: (_, ApplicationStore applicationStore,
                  AuthenticationStore authenticationStore, __) {
                return ApplicationService(
                    applicationStore, authenticationStore);
              },
            ),
          ],
          child: App.forTesting(navigatorObserver: mockNavigatorObserver),
        ),
      );

      await tester.pumpWidget(testApp);
      await tester.pump(const Duration(seconds: 1));
    }

    testWidgets('Landing Screen works properly', (WidgetTester tester) async {
      await _buildTestApp(tester);

      expect(find.text('To provide a cheap on-ramp for everything crypto.'),
          findsOneWidget);
      expect(find.byType(Image), findsOneWidget);
    });

    testWidgets('Pressing Login button navigates to Sign In screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.tap(find.byKey(LandingScreen.navigateToSignInButtonKey));
      await tester.pumpAndSettle();

      verify(mockNavigatorObserver.didPush(any, any));
      expect(find.byType(SignInScreen), findsOneWidget);
    });

    testWidgets('Pressing Register button navigates to Sign Up screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.tap(find.byKey(LandingScreen.navigateToSignUpButtonKey));
      await tester.pumpAndSettle();

      verify(mockNavigatorObserver.didPush(any, any));
      expect(find.byType(SignUpScreen), findsOneWidget);
    });
  });
}
