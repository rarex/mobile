import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:mockito/mockito.dart';
import 'package:graphql/client.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/api/authentication_api.dart';
import 'package:rarex/models/account.dart';
import 'package:rarex/models/session.dart';
import 'package:rarex/models/user.dart';
import 'package:rarex/screens/accounts_screen.dart';
import 'package:rarex/screens/landing_screen.dart';
import 'package:rarex/screens/sign_in_screen.dart';
import 'package:rarex/screens/user_confirmation_screen.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/utils/app_config.dart';
import 'package:rarex/widgets/app.dart';

import '../mocks.dart';

void main() {
  group('Sign In Screen tests', () {
    NavigatorObserver mockNavigatorObserver;
    AccountingApi mockAccountingApi;
    AuthenticationApi mockAuthenticationApi;

    setUp(() {
      mockNavigatorObserver = MockNavigatorObserver();
      mockAccountingApi = MockAccountingApi();
      mockAuthenticationApi = MockAuthenticationApi();
      SharedPreferences.setMockInitialValues(<String, dynamic>{});
    });

    Future<void> _buildTestApp(WidgetTester tester) async {
      final AppConfig appConfig = AppConfig(
        apiHost: 'http://localhost:4000/api',
        appName: 'Rarex TEST',
        flavorName: 'test',
      );

      final Widget testApp = Provider<AppConfig>.value(
        value: appConfig,
        child: MultiProvider(
          providers: <SingleChildCloneableWidget>[
            Provider<ApplicationStore>(
              builder: (_) => ApplicationStore(),
            ),
            Provider<AuthenticationStore>(
              builder: (_) => AuthenticationStore(),
            ),
            Provider<GlobalKey<NavigatorState>>(
              builder: (_) => GlobalKey<NavigatorState>(),
            ),
            ProxyProvider2<ApplicationStore, AuthenticationStore,
                ApplicationService>(
              builder: (_, ApplicationStore applicationStore,
                  AuthenticationStore authenticationStore, __) {
                return ApplicationService(
                    applicationStore, authenticationStore);
              },
            ),
          ],
          child: App.forTesting(
            navigatorObserver: mockNavigatorObserver,
            customAccountingApi: mockAccountingApi,
            customAuthenticationApi: mockAuthenticationApi,
          ),
        ),
      );

      await tester.pumpWidget(testApp);
      await tester.tap(find.byKey(LandingScreen.navigateToSignInButtonKey));
      await tester.pumpAndSettle();
    }

    testWidgets(
        'Submitting form with valid data and confirmed user transitions to Accounts Screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      final List<Account> accounts = <Account>[];
      final Session session = Session(
        successful: true,
        authenticationToken: 'abcd1234',
        user: User(
            id: 1234,
            email: 'test@example.com',
            firstName: 'Test',
            lastName: 'User',
            registeredAt: DateTime.parse('2019-06-01T08:00:20'),
            confirmedAt: DateTime.parse('2019-06-01T08:00:20')),
      );

      when(mockAccountingApi.listAccounts()).thenAnswer((_) {
        return Future<List<Account>>.value(accounts);
      });

      when(mockAuthenticationApi.signIn(any, any)).thenAnswer((_) {
        return Future<Session>.value(session);
      });

      await tester.enterText(find.byKey(SignInFormState.emailTextFormFieldKey),
          'test@example.com');
      await tester.enterText(
          find.byKey(SignInFormState.passwordTextFormFieldKey), 'password');
      await tester.tap(find.byKey(SignInFormState.signInButtonKey));
      await tester.pumpAndSettle();

      verify(mockAuthenticationApi.signIn(any, any));
      verify(mockNavigatorObserver.didPop(any, any));
      expect(find.byType(SignInScreen), findsNothing);
      expect(find.byType(AccountsScreen), findsOneWidget);
    });
    testWidgets(
        'Submitting form with valid data and non-confirmed user transitions to User Confirmation Screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      final Session session = Session(
        successful: true,
        authenticationToken: 'abcd1234',
        user: User(
            id: 1234,
            email: 'test@example.com',
            registeredAt: DateTime.parse('2019-06-01T08:00:20')),
      );
      when(mockAuthenticationApi.signIn(any, any))
          .thenAnswer((_) => Future<Session>.value(session));

      await tester.enterText(find.byKey(SignInFormState.emailTextFormFieldKey),
          'test@example.com');
      await tester.enterText(
          find.byKey(SignInFormState.passwordTextFormFieldKey), 'password');
      await tester.tap(find.byKey(SignInFormState.signInButtonKey));
      await tester.pumpAndSettle();

      verify(mockAuthenticationApi.signIn(any, any));
      verify(mockNavigatorObserver.didPop(any, any));
      expect(find.byType(SignInScreen), findsNothing);
      expect(find.byType(UserConfirmationScreen), findsOneWidget);
    });

    testWidgets('Submitting form with non-existing user returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      final Session session = Session(
        successful: false,
        errors: <GraphQLError>[
          GraphQLError(message: 'no user with matching credentials found'),
        ],
      );
      when(mockAuthenticationApi.signIn(any, any))
          .thenAnswer((_) => Future<Session>.value(session));

      await tester.enterText(find.byKey(SignInFormState.emailTextFormFieldKey),
          'test@example.com');
      await tester.enterText(
          find.byKey(SignInFormState.passwordTextFormFieldKey), 'password');
      await tester.tap(find.byKey(SignInFormState.signInButtonKey));
      await tester.pumpAndSettle();

      verify(mockAuthenticationApi.signIn(any, any));
      verifyNever(mockNavigatorObserver.didPop(any, any));
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.byType(SnackBar), findsOneWidget);
      expect(
          find.text('no user with matching credentials found'), findsOneWidget);
    });

    testWidgets(
        'Submitting form while entering an invalid email address returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.enterText(
          find.byKey(SignInFormState.emailTextFormFieldKey), 'test@example');
      await tester.enterText(
          find.byKey(SignInFormState.passwordTextFormFieldKey), 'password');
      await tester.tap(find.byKey(SignInFormState.signInButtonKey));
      await tester.pumpAndSettle();

      expect(find.byType(SignInScreen), findsOneWidget);
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.text('Please enter a valid email address'), findsOneWidget);
      expect(find.text('Please enter your password'), findsNothing);
    });

    testWidgets('Submitting form without entering anything returns error',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.tap(find.byKey(SignInFormState.signInButtonKey));
      await tester.pumpAndSettle();

      expect(find.byType(SignInScreen), findsOneWidget);
      expect(find.byType(UserConfirmationScreen), findsNothing);
      expect(find.text('Please enter your email address'), findsOneWidget);
      expect(find.text('Please enter your password'), findsOneWidget);
    });
  });
}
