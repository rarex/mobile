import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mockito/mockito.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/models/account.dart';
import 'package:rarex/screens/accounts_screen.dart';
import 'package:rarex/screens/landing_screen.dart';
import 'package:rarex/screens/settings_screen.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/utils/app_config.dart';
import 'package:rarex/widgets/app.dart';

import '../mocks.dart';

void main() {
  group('Settings Screen tests', () {
    AccountingApi mockAccountingApi;

    setUp(() {
      mockAccountingApi = MockAccountingApi();

      final List<Account> accounts = <Account>[];
      when(mockAccountingApi.listAccounts()).thenAnswer((_) {
        return Future<List<Account>>.value(accounts);
      });

      const String sessionString = '''
      {
        "successful":true,
        "authenticationToken":"abcd1234",
        "user":{
          "id":"1",
          "email":"test@example.com",
          "firstName":"Test",
          "lastName":"User",
          "registeredAt":"2019-06-01 08:00:20.000",
          "confirmedAt":"2019-06-01 08:00:20.000"
        }
      }
      ''';

      SharedPreferences.setMockInitialValues(<String, dynamic>{
        'flutter.currentSession': sessionString,
      });
    });

    Future<void> _buildTestApp(WidgetTester tester) async {
      final AppConfig appConfig = AppConfig(
        apiHost: 'http://localhost:4000/api',
        appName: 'Rarex TEST',
        flavorName: 'test',
      );

      final Widget testApp = Provider<AppConfig>.value(
        value: appConfig,
        child: MultiProvider(
          providers: <SingleChildCloneableWidget>[
            Provider<ApplicationStore>(
              builder: (_) => ApplicationStore(),
            ),
            Provider<AuthenticationStore>(
              builder: (_) => AuthenticationStore(),
            ),
            Provider<GlobalKey<NavigatorState>>(
              builder: (_) => GlobalKey<NavigatorState>(),
            ),
            ProxyProvider2<ApplicationStore, AuthenticationStore,
                ApplicationService>(
              builder: (_, ApplicationStore applicationStore,
                  AuthenticationStore authenticationStore, __) {
                return ApplicationService(
                    applicationStore, authenticationStore);
              },
            ),
          ],
          child: App.forTesting(
            customAccountingApi: mockAccountingApi,
          ),
        ),
      );

      await tester.pumpWidget(testApp);
      await tester.pump(const Duration(seconds: 1));
      await tester
          .tap(find.widgetWithIcon(BottomNavigationBar, Icons.settings));
      await tester.pumpAndSettle();
    }

    testWidgets('Settings Screen displays properly',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      expect(find.byType(AccountsScreen), findsNothing);
      expect(find.byType(SettingsScreen), findsOneWidget);
    });

    testWidgets('Pressing Sign Out button redirects back to Landing Screen',
        (WidgetTester tester) async {
      await _buildTestApp(tester);

      await tester.tap(find.byKey(SettingsScreen.signOutButtonKey));
      await tester.pumpAndSettle();

      expect(find.byType(SettingsScreen), findsNothing);
      expect(find.byType(LandingScreen), findsOneWidget);
    });
  });
}
