import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:rarex/widgets/main_view.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/utils/app_config.dart';

void main() {
  Future<void> _buildTestApp(WidgetTester tester) async {
    final AppConfig appConfig = AppConfig(
      apiHost: 'http://localhost:4000/api',
      appName: 'Rarex TEST',
      flavorName: 'test',
    );

    final Widget testApp = Provider<AppConfig>.value(
      value: appConfig,
      child: MultiProvider(
        providers: <SingleChildCloneableWidget>[
          Provider<ApplicationStore>(
            builder: (_) => ApplicationStore.forTesting(),
          ),
          Provider<AuthenticationStore>(
            builder: (_) => AuthenticationStore(),
          ),
          Provider<GlobalKey<NavigatorState>>(
            builder: (_) => GlobalKey<NavigatorState>(),
          ),
          ProxyProvider2<ApplicationStore, AuthenticationStore,
              ApplicationService>(
            builder: (_, ApplicationStore applicationStore,
                AuthenticationStore authenticationStore, __) {
              return ApplicationService(applicationStore, authenticationStore);
            },
          ),
        ],
        child: MainView(),
      ),
    );

    await tester.pumpWidget(testApp);
  }

  testWidgets('MainView displays loading circle when app has not loaded yet',
      (WidgetTester tester) async {
    await _buildTestApp(tester);

    expect(find.byType(CircularProgressIndicator), findsOneWidget);
  });
}
