import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:rarex/services/application_service.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/widgets/app.dart';
import 'package:rarex/utils/app_config.dart';

void main() {
  final AppConfig appConfig = AppConfig(
    appName: 'Rarex DEV',
    flavorName: 'development',
    apiHost: 'https://staging-api.rarex.xyz/api',
  );

  final Widget rarex = Provider<AppConfig>.value(
    value: appConfig,
    child: MyApp(),
  );

  runApp(rarex);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildCloneableWidget>[
        Provider<ApplicationStore>(
          builder: (_) => ApplicationStore(),
        ),
        Provider<AuthenticationStore>(
          builder: (_) => AuthenticationStore(),
        ),
        Provider<GlobalKey<NavigatorState>>(
          builder: (_) => GlobalKey<NavigatorState>(),
        ),
        ProxyProvider2<ApplicationStore, AuthenticationStore,
            ApplicationService>(
          builder: (_, ApplicationStore applicationStore,
              AuthenticationStore authenticationStore, __) {
            return ApplicationService(applicationStore, authenticationStore);
          },
        ),
      ],
      child: App(),
    );
  }
}
