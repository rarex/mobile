import 'package:mobx/mobx.dart';

import 'package:rarex/models/session.dart';
import 'package:rarex/models/user.dart';

part 'authentication_store.g.dart';

class AuthenticationStore = AuthenticationStoreBase with _$AuthenticationStore;

abstract class AuthenticationStoreBase with Store {
  @observable
  bool authenticated = false;

  @observable
  String authenticationToken = '';

  @observable
  User currentUser = User();

  @observable
  bool loading = false;

  @action
  void authenticationRequest() {
    loading = true;
  }

  @action
  void authenticationSuccess(Session session) {
    authenticated = true;
    authenticationToken = session.authenticationToken;
    currentUser = session.user;
    loading = false;
  }

  @action
  void authenticationFailure() {
    loading = false;
  }

  @action
  void signOut() {
    authenticated = false;
    authenticationToken = '';
    currentUser = User();
  }

  @action
  void confirmationRequest() {
    loading = true;
  }

  @action
  void confirmationSuccess(User user) {
    currentUser = user;
    loading = false;
  }

  @action
  void confirmationFailure() {
    loading = false;
  }
}
