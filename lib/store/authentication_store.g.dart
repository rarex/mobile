// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$AuthenticationStore on AuthenticationStoreBase, Store {
  final _$authenticatedAtom =
      Atom(name: 'AuthenticationStoreBase.authenticated');

  @override
  bool get authenticated {
    _$authenticatedAtom.context.enforceReadPolicy(_$authenticatedAtom);
    _$authenticatedAtom.reportObserved();
    return super.authenticated;
  }

  @override
  set authenticated(bool value) {
    _$authenticatedAtom.context.conditionallyRunInAction(() {
      super.authenticated = value;
      _$authenticatedAtom.reportChanged();
    }, _$authenticatedAtom, name: '${_$authenticatedAtom.name}_set');
  }

  final _$authenticationTokenAtom =
      Atom(name: 'AuthenticationStoreBase.authenticationToken');

  @override
  String get authenticationToken {
    _$authenticationTokenAtom.context
        .enforceReadPolicy(_$authenticationTokenAtom);
    _$authenticationTokenAtom.reportObserved();
    return super.authenticationToken;
  }

  @override
  set authenticationToken(String value) {
    _$authenticationTokenAtom.context.conditionallyRunInAction(() {
      super.authenticationToken = value;
      _$authenticationTokenAtom.reportChanged();
    }, _$authenticationTokenAtom,
        name: '${_$authenticationTokenAtom.name}_set');
  }

  final _$currentUserAtom = Atom(name: 'AuthenticationStoreBase.currentUser');

  @override
  User get currentUser {
    _$currentUserAtom.context.enforceReadPolicy(_$currentUserAtom);
    _$currentUserAtom.reportObserved();
    return super.currentUser;
  }

  @override
  set currentUser(User value) {
    _$currentUserAtom.context.conditionallyRunInAction(() {
      super.currentUser = value;
      _$currentUserAtom.reportChanged();
    }, _$currentUserAtom, name: '${_$currentUserAtom.name}_set');
  }

  final _$loadingAtom = Atom(name: 'AuthenticationStoreBase.loading');

  @override
  bool get loading {
    _$loadingAtom.context.enforceReadPolicy(_$loadingAtom);
    _$loadingAtom.reportObserved();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.context.conditionallyRunInAction(() {
      super.loading = value;
      _$loadingAtom.reportChanged();
    }, _$loadingAtom, name: '${_$loadingAtom.name}_set');
  }

  final _$AuthenticationStoreBaseActionController =
      ActionController(name: 'AuthenticationStoreBase');

  @override
  void authenticationRequest() {
    final _$actionInfo =
        _$AuthenticationStoreBaseActionController.startAction();
    try {
      return super.authenticationRequest();
    } finally {
      _$AuthenticationStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void authenticationSuccess(Session session) {
    final _$actionInfo =
        _$AuthenticationStoreBaseActionController.startAction();
    try {
      return super.authenticationSuccess(session);
    } finally {
      _$AuthenticationStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void authenticationFailure() {
    final _$actionInfo =
        _$AuthenticationStoreBaseActionController.startAction();
    try {
      return super.authenticationFailure();
    } finally {
      _$AuthenticationStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void signOut() {
    final _$actionInfo =
        _$AuthenticationStoreBaseActionController.startAction();
    try {
      return super.signOut();
    } finally {
      _$AuthenticationStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void confirmationRequest() {
    final _$actionInfo =
        _$AuthenticationStoreBaseActionController.startAction();
    try {
      return super.confirmationRequest();
    } finally {
      _$AuthenticationStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void confirmationSuccess(User user) {
    final _$actionInfo =
        _$AuthenticationStoreBaseActionController.startAction();
    try {
      return super.confirmationSuccess(user);
    } finally {
      _$AuthenticationStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void confirmationFailure() {
    final _$actionInfo =
        _$AuthenticationStoreBaseActionController.startAction();
    try {
      return super.confirmationFailure();
    } finally {
      _$AuthenticationStoreBaseActionController.endAction(_$actionInfo);
    }
  }
}
