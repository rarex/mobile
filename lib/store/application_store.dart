import 'package:mobx/mobx.dart';

part 'application_store.g.dart';

class ApplicationStore = ApplicationStoreBase with _$ApplicationStore;

abstract class ApplicationStoreBase with Store {
  ApplicationStoreBase() {
    loading = false;
  }

  ApplicationStoreBase.forTesting() {
    loading = true;
  }

  @observable
  bool loading;

  @action
  void initializeRequest() {
    loading = true;
  }

  @action
  void initializeSuccess() {
    loading = false;
  }
}
