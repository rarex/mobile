import 'package:mobx/mobx.dart';

import 'package:rarex/models/account.dart';

part 'accounting_store.g.dart';

class AccountingStore = AccountingStoreBase with _$AccountingStore;

abstract class AccountingStoreBase with Store {
  @observable
  List<Account> accounts = <Account>[];

  @observable
  bool loading = false;

  @action
  void listAccountsRequest() {
    loading = true;
  }

  @action
  void listAccountsSuccess(List<Account> userAccounts) {
    accounts = userAccounts;
    loading = false;
  }
}
