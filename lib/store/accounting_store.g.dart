// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'accounting_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$AccountingStore on AccountingStoreBase, Store {
  final _$accountsAtom = Atom(name: 'AccountingStoreBase.accounts');

  @override
  List<Account> get accounts {
    _$accountsAtom.context.enforceReadPolicy(_$accountsAtom);
    _$accountsAtom.reportObserved();
    return super.accounts;
  }

  @override
  set accounts(List<Account> value) {
    _$accountsAtom.context.conditionallyRunInAction(() {
      super.accounts = value;
      _$accountsAtom.reportChanged();
    }, _$accountsAtom, name: '${_$accountsAtom.name}_set');
  }

  final _$loadingAtom = Atom(name: 'AccountingStoreBase.loading');

  @override
  bool get loading {
    _$loadingAtom.context.enforceReadPolicy(_$loadingAtom);
    _$loadingAtom.reportObserved();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.context.conditionallyRunInAction(() {
      super.loading = value;
      _$loadingAtom.reportChanged();
    }, _$loadingAtom, name: '${_$loadingAtom.name}_set');
  }

  final _$AccountingStoreBaseActionController =
      ActionController(name: 'AccountingStoreBase');

  @override
  void listAccountsRequest() {
    final _$actionInfo = _$AccountingStoreBaseActionController.startAction();
    try {
      return super.listAccountsRequest();
    } finally {
      _$AccountingStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void listAccountsSuccess(List<Account> userAccounts) {
    final _$actionInfo = _$AccountingStoreBaseActionController.startAction();
    try {
      return super.listAccountsSuccess(userAccounts);
    } finally {
      _$AccountingStoreBaseActionController.endAction(_$actionInfo);
    }
  }
}
