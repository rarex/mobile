// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'application_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars

mixin _$ApplicationStore on ApplicationStoreBase, Store {
  final _$loadingAtom = Atom(name: 'ApplicationStoreBase.loading');

  @override
  bool get loading {
    _$loadingAtom.context.enforceReadPolicy(_$loadingAtom);
    _$loadingAtom.reportObserved();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.context.conditionallyRunInAction(() {
      super.loading = value;
      _$loadingAtom.reportChanged();
    }, _$loadingAtom, name: '${_$loadingAtom.name}_set');
  }

  final _$ApplicationStoreBaseActionController =
      ActionController(name: 'ApplicationStoreBase');

  @override
  void initializeRequest() {
    final _$actionInfo = _$ApplicationStoreBaseActionController.startAction();
    try {
      return super.initializeRequest();
    } finally {
      _$ApplicationStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void initializeSuccess() {
    final _$actionInfo = _$ApplicationStoreBaseActionController.startAction();
    try {
      return super.initializeSuccess();
    } finally {
      _$ApplicationStoreBaseActionController.endAction(_$actionInfo);
    }
  }
}
