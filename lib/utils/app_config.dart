import 'package:meta/meta.dart';

class AppConfig {
  AppConfig({
    @required this.appName,
    @required this.flavorName,
    @required this.apiHost,
  });

  String appName;
  String flavorName;
  String apiHost;
}
