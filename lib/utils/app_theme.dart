import 'package:flutter/material.dart';

// ignore: avoid_classes_with_only_static_members
class AppTheme {
  static ThemeData light = ThemeData(
    appBarTheme: const AppBarTheme(
      iconTheme: IconThemeData(
        color: Color(0xff595959),
      ),
      textTheme: TextTheme(
        title: TextStyle(
          color: Color(0xff595959),
          fontSize: 20.0,
          fontWeight: FontWeight.w500,
        ),
      ),
    ),
    scaffoldBackgroundColor: const Color(0xffFCFCFF),
    backgroundColor: const Color(0xffFCFCFF),
    brightness: Brightness.light,
    buttonTheme: ButtonThemeData(
      colorScheme: ColorScheme.light(
        primary: const Color(0xff595959),
      ),
      height: 45.0,
      textTheme: ButtonTextTheme.primary,
    ),
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: const Color(0xffC4C4C4),
        ),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: const Color(0xffC4C4C4),
        ),
      ),
      labelStyle: const TextStyle(
        color: Color(0xff595959),
      ),
    ),
    primaryColor: const Color(0xffFCFCFF),
  );
}
