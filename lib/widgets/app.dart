import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/api/authentication_api.dart';
import 'package:rarex/services/application_service.dart';
import 'package:rarex/services/authentication_service.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/widgets/main_view.dart';
import 'package:rarex/utils/app_config.dart';
import 'package:rarex/utils/app_theme.dart';

// ignore: must_be_immutable
class App extends StatefulWidget {
  App();

  App.forTesting({
    this.navigatorObserver,
    this.customAccountingApi,
    this.customAuthenticationApi,
  });

  NavigatorObserver navigatorObserver;

  AccountingApi customAccountingApi;
  AuthenticationApi customAuthenticationApi;

  @override
  AppState createState() {
    return AppState();
  }
}

class AppState extends State<App> {
  @override
  void initState() {
    super.initState();

    Future<void>.microtask(() async {
      final ApplicationService applicationService =
          Provider.of<ApplicationService>(context);
      applicationService.initializeApp();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) {
        final AppConfig appConfig = Provider.of<AppConfig>(context);
        final AuthenticationStore authenticationStore =
            Provider.of<AuthenticationStore>(context);

        Link link;
        final HttpLink httpLink = HttpLink(
          uri: appConfig.apiHost,
        );

        if (authenticationStore.authenticationToken.isNotEmpty) {
          final AuthLink authLink = AuthLink(
            getToken: () => 'Bearer ${authenticationStore.authenticationToken}',
          );

          // ignore: avoid_as
          link = authLink.concat(httpLink as Link);
        } else {
          // ignore: avoid_as
          link = httpLink as Link;
        }

        final ValueNotifier<GraphQLClient> client = ValueNotifier<GraphQLClient>(
          GraphQLClient(
            cache: InMemoryCache(),
            // ignore: avoid_as
            link: link,
          ),
        );

        final AccountingApi accountingApi = AccountingApi(client.value);
        final AuthenticationApi authenticationApi = AuthenticationApi(client.value);

        final GlobalKey<NavigatorState> navigatorKey =
            Provider.of<GlobalKey<NavigatorState>>(context);

        return GraphQLProvider(
          client: client,
          child: MultiProvider(
            providers: <SingleChildCloneableWidget>[
              Provider<AccountingApi>.value(
                  value: widget.customAccountingApi ?? accountingApi),
              Provider<AuthenticationApi>.value(
                  value: widget.customAuthenticationApi ?? authenticationApi),
              ProxyProvider<AuthenticationApi, AuthenticationService>(
                builder: (_, AuthenticationApi authenticationApi, __) {
                  return AuthenticationService(
                    api: authenticationApi,
                    authenticationStore: authenticationStore,
                    navigatorKey: navigatorKey,
                  );
                },
              ),
            ],
            child: MaterialApp(
              title: appConfig.appName,
              theme: AppTheme.light,
              // ignore: always_specify_types
              navigatorObservers: <NavigatorObserver>[widget.navigatorObserver ?? RouteObserver<PageRoute>()],
              navigatorKey: navigatorKey,
              home: MainView(),
            ),
          ),
        );
      },
    );
  }
}
