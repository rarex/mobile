import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:rarex/models/user.dart';
import 'package:rarex/screens/landing_screen.dart';
import 'package:rarex/screens/user_confirmation_screen.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';
import 'package:rarex/widgets/authenticated_view.dart';

class MainView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ApplicationStore applicationStore =
        Provider.of<ApplicationStore>(context);
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(
      builder: (_) {
        if (applicationStore.loading) {
          return Center(
            child: const CircularProgressIndicator(),
          );
        }

        if (authenticationStore.authenticated &&
            authenticationStore.authenticationToken.isNotEmpty &&
            authenticationStore.currentUser != User()) {
          if (authenticationStore.currentUser.confirmedAt == null) {
            return UserConfirmationScreen();
          }

          return AuthenticatedView();
        }

        return LandingScreen();
      },
    );
  }
}
