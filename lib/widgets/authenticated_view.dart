import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/screens/accounts_screen.dart';
import 'package:rarex/screens/settings_screen.dart';
import 'package:rarex/services/accounting_service.dart';
import 'package:rarex/store/accounting_store.dart';
import 'package:rarex/widgets/bottom_bar.dart';

class AuthenticatedView extends StatefulWidget {
  @override
  AuthenticatedViewState createState() {
    return AuthenticatedViewState();
  }
}

class AuthenticatedViewState extends State<AuthenticatedView>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  int selectedTab = 0;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    tabController.dispose();
  }

  Widget _buildTabViews() {
    return TabBarView(
      controller: tabController,
      physics: const BouncingScrollPhysics(),
      children: <Widget>[
        AccountsScreen(),
        SettingsScreen(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: <SingleChildCloneableWidget>[
        Provider<AccountingStore>(
          builder: (_) => AccountingStore(),
        ),
        ProxyProvider2<AccountingStore, AccountingApi, AccountingService>(
          builder: (_, AccountingStore accountingStore, AccountingApi accountingApi, __) {
            return AccountingService(
              accountingStore: accountingStore,
              api: accountingApi,
            );
          },
        ),
      ],
      child: Scaffold(
        body: _buildTabViews(),
        bottomNavigationBar: BottomBar(
          currentIndex: selectedTab,
          onTap: (int newIndex) {
            setState(() {
              selectedTab = newIndex;
              tabController.index = newIndex;
            });
          },
        ),
      ),
    );
  }
}
