import 'package:flutter/material.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    @required this.currentIndex,
    @required this.onTap,
  });

  final int currentIndex;
  final ValueChanged<int> onTap;

  List<BottomNavigationBarItem> _bottomBarItems() {
    return <BottomNavigationBarItem>[
      const BottomNavigationBarItem(
        icon: Icon(Icons.account_balance_wallet),
        title: Text('Accounts'),
      ),
      const BottomNavigationBarItem(
        icon: Icon(Icons.settings),
        title: Text('Settings'),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.black,
      ),
      child: BottomNavigationBar(
        currentIndex: currentIndex,
        items: _bottomBarItems(),
        onTap: onTap,
      ),
    );
  }
}
