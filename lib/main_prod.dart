import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'package:rarex/main.dart';
import 'package:rarex/utils/app_config.dart';

void main() {
  final AppConfig appConfig = AppConfig(
    appName: 'Rarex',
    flavorName: 'production',
    apiHost: 'https://api.rarex.xyz/api',
  );

  final Widget rarex = Provider<AppConfig>.value(
    value: appConfig,
    child: MyApp(),
  );

  runApp(rarex);
}
