import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

import 'package:rarex/services/authentication_service.dart';
import 'package:rarex/store/authentication_store.dart';

class SignInScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Log In'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20.0),
        children: <Widget>[SignInForm()],
      ),
    );
  }
}

class SignInForm extends StatefulWidget {
  @override
  SignInFormState createState() {
    return SignInFormState();
  }
}

class SignInFormState extends State<SignInForm> {
  static const Key emailTextFormFieldKey = Key('emailTextFormField');
  static const Key passwordTextFormFieldKey = Key('passwordTextFormField');
  static const Key signInButtonKey = Key('signInButton');
  static const Key forgotPasswordButtonKey = Key('forgotPasswordButton');

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final RegExp emailRegex =
      RegExp(r'^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$');

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _emailField(),
          const SizedBox(height: 20.0),
          _passwordField(),
          const SizedBox(height: 15.0),
          _signInButton(),
          const SizedBox(height: 5.0),
          _forgotPasswordButton(),
        ],
      ),
    );
  }

  Widget _emailField() {
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(
      builder: (_) {
        return TextFormField(
          key: emailTextFormFieldKey,
          controller: _emailController,
          decoration: InputDecoration(
            labelText: 'Email Address',
            hintText: 'Enter your email address',
          ),
          enabled: !authenticationStore.loading,
          keyboardType: TextInputType.emailAddress,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Please enter your email address';
            } else if (!emailRegex.hasMatch(value)) {
              return 'Please enter a valid email address';
            }

            return null;
          },
        );
      },
    );
  }

  Widget _passwordField() {
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(
      builder: (_) {
        return TextFormField(
          key: passwordTextFormFieldKey,
          controller: _passwordController,
          decoration: InputDecoration(
            labelText: 'Password',
            hintText: 'Enter your password',
          ),
          enabled: !authenticationStore.loading,
          keyboardType: TextInputType.text,
          obscureText: true,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Please enter your password';
            }

            return null;
          },
        );
      },
    );
  }

  Widget _signInButton() {
    final AuthenticationService authenticationService =
        Provider.of<AuthenticationService>(context);
    final ScaffoldState scaffoldState = Scaffold.of(context);

    return Consumer<AuthenticationStore>(
      child: const Text('Log In'),
      builder: (BuildContext context, AuthenticationStore authenticationStore,
          Widget child) {
        return Observer(
          builder: (_) {
            return Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    key: signInButtonKey,
                    onPressed: authenticationStore.loading
                        ? null
                        : () {
                            if (_formKey.currentState.validate()) {
                              authenticationService.signIn(_emailController,
                                  _passwordController, scaffoldState);
                            }
                          },
                    child: authenticationStore.loading
                        ? const CircularProgressIndicator()
                        : child,
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }

  Widget _forgotPasswordButton() {
    return Row(
      children: const <Widget>[
        Expanded(
          child: FlatButton(
            key: forgotPasswordButtonKey,
            onPressed: null,
            child: Text('Forgot your password?'),
          ),
        ),
      ],
    );
  }
}
