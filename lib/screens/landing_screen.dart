import 'package:flutter/material.dart';

import 'package:rarex/screens/sign_in_screen.dart';
import 'package:rarex/screens/sign_up_screen.dart';

class LandingScreen extends StatelessWidget {
  static const Key navigateToSignInButtonKey = Key('navigateToSignInButton');
  static const Key navigateToSignUpButtonKey = Key('navigateToSignUpButton');

  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQueryData = MediaQuery.of(context);
    final double deviceHeight = mediaQueryData.size.height;
    final double deviceWidth = mediaQueryData.size.width;

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all((deviceHeight + deviceWidth) / 50.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: (deviceHeight + deviceWidth) / 10.0),
            _appLogo(),
            _tagLine(context),
            SizedBox(height: (deviceHeight + deviceWidth) / 8.0),
            _signUpButton(context),
            const SizedBox(height: 15.0),
            _signInButton(context),
          ],
        ),
      ),
    );
  }

  Widget _appLogo() {
    return Center(
      child: Image.asset('assets/images/logo.png'),
    );
  }

  Widget _tagLine(BuildContext context) {
    final MediaQueryData mediaQueryData = MediaQuery.of(context);
    final double deviceHeight = mediaQueryData.size.height;
    final double deviceWidth = mediaQueryData.size.width;

    return Container(
      padding: const EdgeInsets.all(10.0),
      constraints: BoxConstraints.expand(
          height: deviceHeight / 8.0, width: deviceWidth / 1.5),
      child: const Text('To provide a cheap on-ramp for everything crypto.',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18.0,
          )),
    );
  }

  Widget _signUpButton(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: RaisedButton(
            key: navigateToSignUpButtonKey,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute<SignUpScreen>(
                  builder: (BuildContext context) {
                return SignUpScreen();
              }));
            },
            child: const Text('Lets get started',
                style: TextStyle(fontSize: 16.0)),
          ),
        ),
      ],
    );
  }

  Widget _signInButton(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: OutlineButton(
            key: navigateToSignInButtonKey,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute<SignInScreen>(
                  builder: (BuildContext context) {
                return SignInScreen();
              }));
            },
            child: const Text('I already have an account',
                style: TextStyle(fontSize: 16.0)),
          ),
        ),
      ],
    );
  }
}
