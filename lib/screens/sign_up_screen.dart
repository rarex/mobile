import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:rarex/services/authentication_service.dart';
import 'package:rarex/store/authentication_store.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create Account'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20.0),
        children: <Widget>[SignUpForm()],
      ),
    );
  }
}

class SignUpForm extends StatefulWidget {
  @override
  SignUpFormState createState() {
    return SignUpFormState();
  }
}

class SignUpFormState extends State<SignUpForm> {
  static const Key emailTextFormFieldKey = Key('emailTextFormField');
  static const Key passwordTextFormFieldKey = Key('passwordTextFormField');
  static const Key passwordConfirmationTextFormFieldKey =
      Key('passwordConfirmationTextFormField');
  static const Key firstNameTextFormFieldKey = Key('firstNameTextFormField');
  static const Key lastNameTextFormFieldKey = Key('lastNameTextFormField');
  static const Key signUpButtonKey = Key('signUpButton');

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmationController =
      TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final RegExp emailRegex =
      RegExp(r'^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$');

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _passwordConfirmationController.dispose();
    _firstNameController.dispose();
    _lastNameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _nameField(
              firstNameTextFormFieldKey, _firstNameController, 'First Name'),
          const SizedBox(height: 20.0),
          _nameField(
              lastNameTextFormFieldKey, _lastNameController, 'Last Name'),
          const SizedBox(height: 20.0),
          _emailField(),
          const SizedBox(height: 20.0),
          _passwordField(),
          const SizedBox(height: 20.0),
          _passwordConfirmationField(),
          const SizedBox(height: 15.0),
          _signUpButton(),
        ],
      ),
    );
  }

  Widget _emailField() {
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(
      builder: (_) {
        return TextFormField(
          key: emailTextFormFieldKey,
          controller: _emailController,
          decoration: InputDecoration(
            labelText: 'Email Address',
            hintText: 'Enter your email address',
          ),
          enabled: !authenticationStore.loading,
          keyboardType: TextInputType.emailAddress,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Please enter your email address';
            } else if (!emailRegex.hasMatch(value)) {
              return 'Please enter a valid email address';
            }

            return null;
          },
        );
      },
    );
  }

  Widget _passwordField() {
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(
      builder: (_) {
        return TextFormField(
          key: passwordTextFormFieldKey,
          controller: _passwordController,
          decoration: InputDecoration(
            labelText: 'Password',
            hintText: 'Enter your password',
          ),
          enabled: !authenticationStore.loading,
          keyboardType: TextInputType.text,
          obscureText: true,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Please enter your password';
            }

            return null;
          },
        );
      },
    );
  }

  Widget _passwordConfirmationField() {
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(
      builder: (_) {
        return TextFormField(
          key: passwordConfirmationTextFormFieldKey,
          controller: _passwordConfirmationController,
          decoration: InputDecoration(
            labelText: 'Re-type your password',
            hintText: 'Enter your password again',
          ),
          enabled: !authenticationStore.loading,
          keyboardType: TextInputType.text,
          obscureText: true,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Please confirm your password';
            } else if (_passwordController.text != value) {
              return 'Password confirmation does not match';
            }

            return null;
          },
        );
      },
    );
  }

  Widget _nameField(
      Key key, TextEditingController controller, String fieldName) {
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(builder: (_) {
      return TextFormField(
        key: key,
        controller: controller,
        decoration: InputDecoration(
          labelText: fieldName,
          hintText: 'Enter your ${fieldName.toLowerCase()}',
        ),
        enabled: !authenticationStore.loading,
        keyboardType: TextInputType.text,
        validator: (String value) {
          if (value.isEmpty) {
            return 'Please enter your ${fieldName.toLowerCase()}';
          }

          return null;
        },
      );
    });
  }

  Widget _signUpButton() {
    final AuthenticationService authenticationService =
        Provider.of<AuthenticationService>(context);
    final ScaffoldState scaffoldState = Scaffold.of(context);

    return Consumer<AuthenticationStore>(
      child: const Text('Create Account'),
      builder: (BuildContext context, AuthenticationStore authenticationStore,
          Widget child) {
        return Observer(builder: (_) {
          return Row(
            children: <Widget>[
              Expanded(
                child: RaisedButton(
                  key: signUpButtonKey,
                  onPressed: authenticationStore.loading
                      ? null
                      : () {
                          if (_formKey.currentState.validate()) {
                            authenticationService.signUp(
                                _emailController,
                                _passwordController,
                                _passwordConfirmationController,
                                _firstNameController,
                                _lastNameController,
                                scaffoldState);
                          }
                        },
                  child: authenticationStore.loading
                      ? const CircularProgressIndicator()
                      : child,
                ),
              ),
            ],
          );
        });
      },
    );
  }
}
