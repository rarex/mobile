import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:rarex/services/authentication_service.dart';
import 'package:rarex/store/authentication_store.dart';

class UserConfirmationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Account Verification'),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20.0),
        children: <Widget>[UserConfirmationForm()],
      ),
    );
  }
}

class UserConfirmationForm extends StatefulWidget {
  @override
  UserConfirmationFormState createState() {
    return UserConfirmationFormState();
  }
}

class UserConfirmationFormState extends State<UserConfirmationForm> {
  static const Key confirmationCodeFormFieldKey =
      Key('confirmationCodeFormField');
  static const Key confirmButtonKey = Key('confirmButton');
  static const Key signOutButtonKey = Key('signOutButton');

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _confirmationCodeController =
      TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _confirmationCodeController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          const Text('We sent an email containing your verification code.'),
          const SizedBox(height: 15.0),
          _confirmationCodeField(),
          const SizedBox(height: 15.0),
          _confirmButton(),
          const SizedBox(height: 15.0),
          _signOutButton(),
        ],
      ),
    );
  }

  Widget _confirmationCodeField() {
    final AuthenticationStore authenticationStore =
        Provider.of<AuthenticationStore>(context);

    return Observer(
      builder: (_) {
        return TextFormField(
          key: confirmationCodeFormFieldKey,
          controller: _confirmationCodeController,
          decoration: InputDecoration(
            labelText: 'Account Verification Code',
            hintText: 'Enter 4-digit code',
          ),
          enabled: !authenticationStore.loading,
          keyboardType: TextInputType.number,
          validator: (String value) {
            if (value.isEmpty) {
              return 'Please enter your account verification code';
            }

            return null;
          },
        );
      },
    );
  }

  Widget _confirmButton() {
    final AuthenticationService authenticationService =
        Provider.of<AuthenticationService>(context);
    final ScaffoldState scaffoldState = Scaffold.of(context);

    return Consumer<AuthenticationStore>(
      child: const Text('Verify Account'),
      builder: (BuildContext context, AuthenticationStore authenticationStore,
          Widget child) {
        return Observer(
          builder: (_) {
            return Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    key: confirmButtonKey,
                    onPressed: authenticationStore.loading
                        ? null
                        : () {
                            if (_formKey.currentState.validate()) {
                              authenticationService.confirmUser(
                                  _confirmationCodeController, scaffoldState);
                            }
                          },
                    child: authenticationStore.loading
                        ? const CircularProgressIndicator()
                        : child,
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }

  Widget _signOutButton() {
    final AuthenticationService authenticationService =
        Provider.of<AuthenticationService>(context);

    return Consumer<AuthenticationStore>(
        child: const Text('Log Out'),
        builder: (BuildContext context, AuthenticationStore authenticationStore,
            Widget child) {
          return Observer(
            builder: (_) {
              return Row(
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      child: child,
                      key: signOutButtonKey,
                      onPressed: authenticationStore.loading
                          ? null
                          : () {
                              authenticationService.signOut();
                            },
                    ),
                  ),
                ],
              );
            },
          );
        });
  }
}
