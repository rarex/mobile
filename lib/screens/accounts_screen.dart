import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:rarex/services/accounting_service.dart';
import 'package:rarex/store/accounting_store.dart';

class AccountsScreen extends StatefulWidget {
  @override
  AccountsScreenState createState() {
    return AccountsScreenState();
  }
}

class AccountsScreenState extends State<AccountsScreen> {
  @override
  void initState() {
    super.initState();

    Future<void>.microtask(() async {
      final AccountingService accountingService = Provider.of<AccountingService>(context);
      await accountingService.listAccounts();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Accounts'),
      ),
      body: Column(
        children: <Widget>[
          _CurrentBalance(),
          Expanded(
            child: _AccountsList(),
          ),
        ],
      ),
    );
  }
}

class _CurrentBalance extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: Center(
        child: Column(
          children: const <Widget>[
            Text(
              '\u20b1 1,234,567.89',
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 10.0),
            Text(
              'CURRENT BALANCE',
              style: TextStyle(
                color: Color(0xff949494),
                fontSize: 12.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _AccountsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final AccountingStore accountingStore = Provider.of<AccountingStore>(context);

    return Observer(
      builder: (_) {
        if (accountingStore.loading) {
          return Center(
            child: const CircularProgressIndicator(),
          );
        }

        if (accountingStore.accounts.isNotEmpty) {
          return Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(color: Theme.of(context).dividerColor),
              ),
            ),
            child: ListView.builder(
              itemCount: accountingStore.accounts.length,
              itemBuilder: (_, int index) {
                return AccountItem(
                  balance: accountingStore.accounts[index].balance.toString(),
                  currencyCode: accountingStore.accounts[index].currency.code,
                  currencyName: accountingStore.accounts[index].currency.name,
                );
              },
            ),
          );
        }

        return Center(
          child: const Text('You have no accounts'),
        );
      }
    );
  }
}

class AccountItem extends StatelessWidget {
  const AccountItem({
    this.balance,
    this.currencyCode,
    this.currencyName,
  });

  final String balance;
  final String currencyCode;
  final String currencyName;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(color: Theme.of(context).dividerColor),
      )),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              const Icon(Icons.attach_money),
              Text(currencyName),
            ],
          ),
          Row(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        balance,
                        style: const TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                      const SizedBox(width: 3.0),
                      Text(
                        currencyCode,
                        style: const TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: const <Widget>[
                      Text(
                        '555,555.55',
                        style: TextStyle(
                          color: Color(0xff949494),
                          fontSize: 12.0,
                        ),
                      ),
                      SizedBox(width: 3.0),
                      Text(
                        'PHP',
                        style: TextStyle(
                          color: Color(0xff949494),
                          fontSize: 12.0,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(width: 5.0),
              const Icon(Icons.arrow_forward),
            ],
          ),
        ],
      ),
    );
  }
}
