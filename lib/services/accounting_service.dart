import 'package:rarex/api/accounting_api.dart';
import 'package:rarex/models/account.dart';
import 'package:rarex/store/accounting_store.dart';

class AccountingService {
  AccountingService({this.accountingStore, this.api});

  AccountingStore accountingStore;
  AccountingApi api;

  Future<void> listAccounts() async {
    accountingStore.listAccountsRequest();

    final List<Account> accounts = await api.listAccounts();
    accountingStore.listAccountsSuccess(accounts);
  }
}
