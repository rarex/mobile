import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:rarex/api/authentication_api.dart';
import 'package:rarex/models/session.dart';
import 'package:rarex/models/user.dart';
import 'package:rarex/store/authentication_store.dart';

class AuthenticationService {
  AuthenticationService(
      {this.api, this.authenticationStore, this.navigatorKey});

  AuthenticationApi api;
  AuthenticationStore authenticationStore;
  GlobalKey<NavigatorState> navigatorKey;
  GlobalKey<ScaffoldState> scaffoldKey;

  Future<void> signIn(
      TextEditingController emailController,
      TextEditingController passwordController,
      ScaffoldState scaffoldState) async {
    authenticationStore.authenticationRequest();
    final Session session =
        await api.signIn(emailController.text, passwordController.text);

    if (session.successful) {
      final SharedPreferences preferences =
          await SharedPreferences.getInstance();
      await preferences.setString('currentSession', session.toString());
      authenticationStore.authenticationSuccess(session);
      navigatorKey.currentState.pop();
    } else {
      passwordController.clear();
      scaffoldState.showSnackBar(SnackBar(
        backgroundColor: Colors.red,
        content: Text(session.errors[0].message),
      ));
      authenticationStore.authenticationFailure();
    }
  }

  Future<void> signOut() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('currentSession');

    authenticationStore.signOut();
  }

  Future<void> signUp(
      TextEditingController emailController,
      TextEditingController passwordController,
      TextEditingController passwordConfirmationController,
      TextEditingController firstNameController,
      TextEditingController lastNameController,
      ScaffoldState scaffoldState) async {
    authenticationStore.authenticationRequest();
    final Session session = await api.signUp(
        emailController.text,
        passwordController.text,
        passwordConfirmationController.text,
        firstNameController.text,
        lastNameController.text);

    if (session.successful) {
      final SharedPreferences preferences =
          await SharedPreferences.getInstance();
      await preferences.setString('currentSession', session.toString());
      authenticationStore.authenticationSuccess(session);
      navigatorKey.currentState.pop();
    } else {
      passwordController.clear();
      passwordConfirmationController.clear();
      scaffoldState.showSnackBar(SnackBar(
        backgroundColor: Colors.red,
        content: Text(session.errors[0].message),
      ));
      authenticationStore.authenticationFailure();
    }
  }

  Future<void> confirmUser(TextEditingController confirmationCodeController,
      ScaffoldState scaffoldState) async {
    authenticationStore.confirmationRequest();
    final User user = await api.confirmUser(confirmationCodeController.text);

    if (user.confirmedAt == null) {
      scaffoldState.showSnackBar(SnackBar(
        backgroundColor: Colors.red,
        content: const Text('Verification code is incorrect'),
      ));
      authenticationStore.confirmationFailure();
    } else {
      final Session currentSession = Session(
        authenticationToken: authenticationStore.authenticationToken,
        user: user,
      );
      final SharedPreferences preferences =
          await SharedPreferences.getInstance();
      await preferences.remove('currentSession');
      await preferences.setString('currentSession', currentSession.toString());
      authenticationStore.confirmationSuccess(user);
    }
  }
}
