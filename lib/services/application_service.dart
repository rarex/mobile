import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:rarex/models/session.dart';
import 'package:rarex/store/application_store.dart';
import 'package:rarex/store/authentication_store.dart';

class ApplicationService {
  ApplicationService(this.applicationStore, this.authenticationStore);

  ApplicationStore applicationStore;
  AuthenticationStore authenticationStore;

  Future<void> initializeApp() async {
    if (authenticationStore.authenticated &&
        authenticationStore.authenticationToken != '') {
      return;
    }

    applicationStore.initializeRequest();

    final SharedPreferences preferences = await SharedPreferences.getInstance();
    final String currentSession = preferences.getString('currentSession');

    if (currentSession != null) {
      final Session session = Session.fromJson(json.decode(currentSession));
      authenticationStore.authenticationSuccess(session);
    }

    applicationStore.initializeSuccess();
  }
}
