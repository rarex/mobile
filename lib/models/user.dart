import 'package:graphql/client.dart';

class User {
  User({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.registeredAt,
    this.confirmedAt,
  });

  User.fromAuthentication(QueryResult result) {
    result.data.forEach((dynamic key, dynamic value) {
      id = int.parse(value['user']['id']);
      email = value['user']['email'];
      firstName = value['user']['firstName'];
      lastName = value['user']['lastName'];
      registeredAt = DateTime.parse(value['user']['insertedAt']);

      if (value['user']['confirmedAt'] != null) {
        confirmedAt = DateTime.parse(value['user']['confirmedAt']);
      }
    });
  }

  User.fromConfirmation(QueryResult result) {
    result.data.forEach((dynamic key, dynamic value) {
      id = int.parse(value['id']);
      email = value['email'];
      firstName = value['firstName'];
      lastName = value['lastName'];
      registeredAt = DateTime.parse(value['insertedAt']);
      confirmedAt = DateTime.parse(value['confirmedAt']);
    });
  }

  User.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    email = json['email'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    registeredAt = DateTime.parse(json['registeredAt']);

    if (json['confirmedAt'] != null) {
      confirmedAt = DateTime.parse(json['confirmedAt']);
    }
  }

  int id;
  String email;
  String firstName;
  String lastName;
  DateTime registeredAt;
  DateTime confirmedAt;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> toBeConverted = <String, dynamic>{
      'id': id.toString(),
      'email': email,
      'firstName': firstName,
      'lastName': lastName,
      'registeredAt': registeredAt.toString(),
    };

    if (confirmedAt != null) {
      toBeConverted['confirmedAt'] = confirmedAt.toString();
    }

    return toBeConverted;
  }
}
