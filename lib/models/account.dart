import 'package:decimal/decimal.dart';
import 'package:graphql/client.dart';

import 'package:rarex/models/currency.dart';

class Account {
  Account({
    this.balance,
    this.currency,
  });

  Decimal balance;
  Currency currency;

  static List<Account> fromAccountsList(QueryResult result) {
    final List<Account> accounts = <Account>[];

    if (result.data != null) {
      result.data['accounts'].forEach((dynamic account) {
        accounts.add(Account(
          balance: Decimal.parse(account['balance']),
          currency: Currency(
            code: account['currency']['code'],
            name: account['currency']['name'],
          ),
        ));
      });
    }

    return accounts;
  }
}
