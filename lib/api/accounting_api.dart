import 'package:graphql/client.dart';

import 'package:rarex/api/api.dart';
import 'package:rarex/models/account.dart';

class AccountingApi extends RarexApi {
  AccountingApi(GraphQLClient client) : super(client);

  Future<List<Account>> listAccounts() async {
    final QueryResult result = await client.query(QueryOptions(
      document: '''
      query ListAccounts {
        accounts {
          balance,
          currency {
            name,
            code
          }
        }
      }
    '''));

    return Account.fromAccountsList(result);
  }
}
