import 'package:graphql/client.dart';

abstract class RarexApi {
  RarexApi(this.client);

  final GraphQLClient client;
}
