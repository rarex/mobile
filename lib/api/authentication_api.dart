import 'package:graphql/client.dart';

import 'package:rarex/api/api.dart';
import 'package:rarex/models/session.dart';
import 'package:rarex/models/user.dart';

class AuthenticationApi extends RarexApi {
  AuthenticationApi(GraphQLClient client) : super(client);

  Future<Session> signIn(String email, String password) async {
    final QueryResult result = await client.mutate(MutationOptions(document: '''
      mutation SignIn(\$input: UserSignInInput!) {
        signIn(input: \$input) {
          authenticationToken,
          user {
            id,
            email,
            firstName,
            lastName,
            insertedAt,
            confirmedAt
          }
        }
      }
      ''', variables: <String, dynamic>{
      'input': <String, String>{
        'email': email,
        'password': password,
      }
    }));

    if (result.hasErrors) {
      return Session.fromAuthenticationFailure(result);
    }

    return Session.fromAuthenticationSuccess(result);
  }

  Future<Session> signUp(String email, String password,
      String passwordConfirmation, String firstName, String lastName) async {
    final QueryResult result = await client.mutate(MutationOptions(document: '''
      mutation SignUp(\$input: UserSignUpInput!) {
        signUp(input: \$input) {
          authenticationToken,
          user {
            id,
            email,
            firstName,
            lastName,
            insertedAt
          }
        }
      }
      ''', variables: <String, dynamic>{
      'input': <String, String>{
        'email': email,
        'password': password,
        'passwordConfirmation': passwordConfirmation,
        'firstName': firstName,
        'lastName': lastName
      }
    }));

    if (result.hasErrors) {
      return Session.fromAuthenticationFailure(result);
    }

    return Session.fromAuthenticationSuccess(result);
  }

  Future<User> confirmUser(String confirmationCode) async {
    final QueryResult result = await client.mutate(MutationOptions(
      document: '''
      mutation ConfirmUser(\$input: UserConfirmationInput!) {
        confirmUser(input: \$input) {
          id,
          email,
          firstName,
          lastName,
          insertedAt,
          confirmedAt
        }
      }
    ''',
      variables: <String, dynamic>{
        'input': <String, String>{
          'confirmationCode': confirmationCode,
        },
      },
    ));

    if (result.hasErrors) {
      return User();
    }

    return User.fromConfirmation(result);
  }
}
