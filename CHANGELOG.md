# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# 0.1.0 (2019-07-03)


### Bug Fixes

* obscure password textfield in sign in form ([f401702](https://gitlab.com/rarex/mobile/commit/f401702))


### Features

* add bottom navigation bar ([8bbbcea](https://gitlab.com/rarex/mobile/commit/8bbbcea))
* add home screen ([3a90271](https://gitlab.com/rarex/mobile/commit/3a90271))
* add landing screen ([b583373](https://gitlab.com/rarex/mobile/commit/b583373))
* add local sign up for new users ([cd44f76](https://gitlab.com/rarex/mobile/commit/cd44f76))
* add static sign in screen ([5d2dd28](https://gitlab.com/rarex/mobile/commit/5d2dd28))
* add user sign up through api ([1bc1e62](https://gitlab.com/rarex/mobile/commit/1bc1e62))
* allow local sign in ([11d1745](https://gitlab.com/rarex/mobile/commit/11d1745))
* allow local sign out ([3ec6de3](https://gitlab.com/rarex/mobile/commit/3ec6de3))
* allow user sign in through api ([a77781f](https://gitlab.com/rarex/mobile/commit/a77781f))
* persist session across app restarts ([a4f22d3](https://gitlab.com/rarex/mobile/commit/a4f22d3))
